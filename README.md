# Domain layer design exercise #
## Domain ##
### Class diagram ###
+ Person
    * ﬁscalId: String
    * name: String
    * salary: Int
    * departments: Department[0..3]
+ Department
    * name:String
    * head: Person
    * bonus: Int
+ Keys
    * Person: ﬁscalId
    * Department: name
+ Restrictions
    * For each department d, d.head.departments includes d
### Operations ###
- listPeople: Returns a list of all the people known by the system. For each person p it returns:
    * p.ﬁscalId
    * p.name
    * The names of the departments of p
    * The name of the department directed by p (if any)
    * The salary of p plus the bonus of the department directed by p (if any)
- promote(ﬁscalId:String, departmentName:String)
    * if there is no person identiﬁed by ﬁscalId throw PersonNotFound
    * if there is no department identiﬁed by departmentName throw DepartmentNotFound
    * if there is any restriction violation throw an exception with a meaningful name
    * Otherwise, make the person identiﬁed by ﬁscalId the head of the department identiﬁed by departmentName
### Tasks ###
1. Design of the service layer (class diagram)
2. Design of the relational DB schema (textual sql)
3. Design of the domain and data access layers using Domain Model ((class diagram + sequence diagram) or Scala
code)
We are not interested in the internal design of the data access layer.
Just draw a comment with the sql code used for each operation
4. Design of the domain and data access layers using Transaction Script ((class diagram + sequence diagram) or
Scala code)
Same as 3
5. (optional, only if 3 & 4 were done in uml) Scala code for the domain layer

### Who do I talk to? ###

* Mohannad El Dafrawy
* Hussein Elbaroudy